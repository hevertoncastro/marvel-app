import { forceHttpsURL } from './forceHttpsURL';

const getHeroAvatarImage = (thumbnail, placeholderImage) => {
  if (thumbnail.path.indexOf('image_not_available') > -1) {
    return placeholderImage;
  }

  return {
    uri: `${forceHttpsURL(thumbnail.path)}.${thumbnail.extension}`,
  };
};

export default getHeroAvatarImage;
