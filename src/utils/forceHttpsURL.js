export const forceHttpsURL = (url) => {
  return url.replace('http://', 'https://');
};
