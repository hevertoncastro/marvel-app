import config from '../settings/config';

export const getResourceURL = (
  orderBy = 'name',
  search = null,
  limit = config.HEROES_LIST.LIMIT,
  page = 0,
) => {
  const offSet = page * limit;
  const baseResource = `${config.API.BASE_URL}`;
  const searchQuery = search ? `&nameStartsWith=${search}` : '';
  const display = `&orderBy=${orderBy}&offset=${offSet}&limit=${limit}`;
  const auth = `&apikey=${config.API.KEY}&ts=${config.API.TS}&hash=${config.API.HASH}`;

  return `${baseResource}?${searchQuery}${display}${auth}`;
};
