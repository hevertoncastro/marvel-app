import colors from '../../settings/colors';

export default {
  flex: 1,
  width: '100%',
  backgroundColor: `${colors.dark}`,
};
