import React from 'react';
import { useNavigation } from '@react-navigation/native';
import getHeroAvatarImage from '../../utils/getHeroAvatarImage';

import {
  ListItemHolder,
  ListItemContent,
  ListItemContentMain,
  ListItemImageHolder,
  ListItemImage,
  ListItemTitleHolder,
  ListItemTitle,
  ListItemContentAside,
  ListItemTag,
  ListItemActions,
} from './styles';

import FavoriteButton from '../FavoriteButton';

const ListItem = ({ item, index }) => {
  const navigation = useNavigation();
  const { id, name, thumbnail, comics, series, stories } = item;
  const placeHolderImage = require('../../assets/images/heroes.png');

  const handleItemPress = () => {
    navigation.navigate('HeroDetails', {
      hero: item,
    });
  };

  return (
    <ListItemHolder>
      <ListItemContent alternated={index % 2}>
        <ListItemContentMain onPress={handleItemPress}>
          <ListItemImageHolder>
            {!!thumbnail && (
              <ListItemImage
                source={getHeroAvatarImage(thumbnail, placeHolderImage)}
                resizeMode="cover"
              />
            )}
          </ListItemImageHolder>
          <ListItemTitleHolder>
            <ListItemTitle size={2} numberOfLines={2}>
              {name}
            </ListItemTitle>
          </ListItemTitleHolder>
        </ListItemContentMain>
        <ListItemContentAside>
          {!!comics?.available && (
            <ListItemTag>comics: {comics?.available}</ListItemTag>
          )}
          {!!series?.available && (
            <ListItemTag>series: {series?.available}</ListItemTag>
          )}
          {!!stories?.available && (
            <ListItemTag>stories: {stories?.available}</ListItemTag>
          )}
        </ListItemContentAside>
      </ListItemContent>

      <ListItemActions alternated={index % 2}>
        <FavoriteButton itemId={id} />
      </ListItemActions>
    </ListItemHolder>
  );
};

export default ListItem;
