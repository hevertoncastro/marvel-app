import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const FavoriteButtonHolder = styled.TouchableOpacity`
  width: 48px;
  height: 48px;
  justify-content: center;
  align-items: center;
  border-radius: 50px;
`;
