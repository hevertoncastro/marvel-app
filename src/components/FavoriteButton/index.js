import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { addFavoriteHero, removeFavoriteHero } from '../../actions/heroes';
import { FavoriteButtonHolder } from './styles';
import colors from '../../settings/colors';

import HeartIcon from '../../assets/icons/heart.svg';
import HeartFilledIcon from '../../assets/icons/heart-filled.svg';

export default ({ itemId }) => {
  const dispatch = useDispatch();
  const favoriteHeroes = useSelector((state) => state.heroes.favoriteHeroes);

  const isFavorite = (id) => {
    return favoriteHeroes?.indexOf(id) > -1;
  };

  const addFavoriteInStorage = async (id) => {
    const updatedFavorites = [...favoriteHeroes];
    updatedFavorites.push(id);
    try {
      const jsonValue = JSON.stringify(updatedFavorites);
      await AsyncStorage.setItem('@favorites_heroes', jsonValue);
    } catch (_) {}
  };

  const removeFavoriteFromStorage = async (id) => {
    const filtered = favoriteHeroes.filter((item) => item !== id);

    try {
      const jsonValue = JSON.stringify(filtered);
      await AsyncStorage.setItem('@favorites_heroes', jsonValue);
    } catch (_) {}
  };

  const handleFavoritePress = (id) => {
    if (isFavorite(id)) {
      dispatch(removeFavoriteHero(id));
      removeFavoriteFromStorage(id);
    } else {
      dispatch(addFavoriteHero(id));
      addFavoriteInStorage(id);
    }
  };

  return (
    <FavoriteButtonHolder onPress={() => handleFavoritePress(itemId)}>
      {isFavorite(itemId) ? (
        <HeartFilledIcon width="100%" height="32" fill={colors.danger} />
      ) : (
        <HeartIcon width="100%" height="32" fill={colors.danger} />
      )}
    </FavoriteButtonHolder>
  );
};
