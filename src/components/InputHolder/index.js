import React from 'react';
import { InputHolder, Input, SearchHolder } from './styles';
import SearchIcon from '../../assets/icons/search.svg';
import colors from '../../settings/colors';

export default ({ onChangeText, placeholder, onSearch }) => {
  return (
    <InputHolder>
      <Input placeholder={placeholder} onChangeText={onChangeText} />
      <SearchHolder onPress={onSearch}>
        <SearchIcon
          style={{ alignSelf: 'flex-end' }}
          width="100%"
          height="28"
          fill={colors.dark}
        />
      </SearchHolder>
    </InputHolder>
  );
};
