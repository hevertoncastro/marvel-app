import React, { useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { Container } from './styles';
import StatusBar from '../../components/StatusBar';
import { setFavoriteHeroes } from '../../actions/heroes';
import config from '../../settings/config';
import { PreLoadImage } from './styles';

const PreLoad = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  useEffect(() => {
    AsyncStorage.getItem('@favorites_heroes').then((heroes) => {
      const favoriteHeroes = heroes != null ? JSON.parse(heroes) : [];
      dispatch(setFavoriteHeroes(favoriteHeroes));

      // Delay just used to
      // hold app loader effect
      setTimeout(() => {
        navigation.reset({
          routes: [{ name: 'HeroesList' }],
        });
      }, config.PRELOADER.NAVIGATION_DELAY);
    });
  }, [dispatch, navigation]);

  return (
    <Container>
      <StatusBar />
      <PreLoadImage
        source={require('../../assets/images/marvel.png')}
        resizeMode="contain"
      />
    </Container>
  );
};

export default PreLoad;
