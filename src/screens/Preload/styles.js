import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const Container = styled.SafeAreaView`
  background-color: ${colors.dark};
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const PreLoadImage = styled.Image`
  width: 100%;
  height: 100px;
`;
