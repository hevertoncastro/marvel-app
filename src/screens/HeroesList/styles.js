import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const Container = styled.SafeAreaView`
  background-color: ${colors.dark};
  flex: 1;
  justify-content: flex-start;
  align-items: center;
`;

export const HeroesListFilters = styled.View`
  width: 100%;
  height: auto;
  justify-content: center;
  align-items: center;
  border-style: solid;
  border-bottom-width: 1px;
  border-bottom-color: ${colors.lightDark};
  background-color: ${colors.dark};
  padding-top: 32px;
  padding-bottom: 20px;
`;

export const HeroesListFilterFeedback = styled.Text`
  font-size: 14px;
  color: ${colors.white};
`;
