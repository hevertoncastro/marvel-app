import React, { useState } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import { fetchHeroes, searchHeroes, sortHeroes } from '../../actions/heroes';
import { FlatList } from 'react-native';
import {
  Container,
  HeroesListFilters,
  HeroesListFilterFeedback,
} from './styles';
import StatusBar from '../../components/StatusBar';
import Header from '../../components/Header';
import InputHolder from '../../components/InputHolder';
import ListItem from '../../components/ListItem';
import config from '../../settings/config';

const HeroList = () => {
  const dispatch = useDispatch();

  const heroes = useSelector((state) => state.heroes.heroes);
  const orderBy = useSelector((state) => state.heroes.orderBy);
  const limit = useSelector((state) => state.heroes.limit);
  const page = useSelector((state) => state.heroes.page);
  const search = useSelector((state) => state.heroes.search);

  const [localSearch, setLocalSearch] = useState(null);
  const [searchVisibility, setSearchVisibility] = useState(false);

  useFocusEffect(
    React.useCallback(() => {
      dispatch(fetchHeroes(orderBy, search, limit, 0));
    }, [dispatch, orderBy, search, limit]),
  );

  const renderItem = ({ item, index }) => (
    <ListItem item={item} index={index} />
  );

  const handleSearch = () => {
    dispatch(searchHeroes(localSearch));
  };

  const handleVisibility = () => {
    if (!searchVisibility) {
      setSearchVisibility(true);
    } else {
      setLocalSearch('');
      setSearchVisibility(false);
      dispatch(searchHeroes(''));
    }
  };

  const handleSortOrder = (sortOrder) => {
    if (sortOrder !== orderBy) {
      dispatch(sortHeroes(sortOrder));
    }
  };

  const loadMoreItems = () => {
    const nextPage = parseInt(page, 10) + 1;
    dispatch(fetchHeroes(orderBy, search, limit, nextPage));
  };

  return (
    <Container>
      <StatusBar />
      <Header
        title="Marvel App"
        sort={() =>
          handleSortOrder(
            orderBy === config.HEROES_LIST.SORT_ASC
              ? config.HEROES_LIST.SORT_DESC
              : config.HEROES_LIST.SORT_ASC,
          )
        }
        sortState={orderBy === config.HEROES_LIST.SORT_ASC}
        search={handleVisibility}
        searchState={searchVisibility}
      />
      {searchVisibility && (
        <HeroesListFilters>
          <InputHolder
            onChangeText={(searchQuery) => setLocalSearch(searchQuery)}
            placeholder={config.HEROES_LIST.SEARCH_PLACEHOLDER}
            onSearch={handleSearch}
          />

          {heroes.length === 0 && (
            <HeroesListFilterFeedback>
              {config.HEROES_LIST.SEARCH_FEEDBACK}
            </HeroesListFilterFeedback>
          )}
        </HeroesListFilters>
      )}

      <FlatList
        data={heroes}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        onEndReached={loadMoreItems}
        onEndReachedThreshold={config.HEROES_LIST.ON_END_THRESHOLD}
        initialNumToRender={config.HEROES_LIST.LIMIT}
      />
    </Container>
  );
};

export default HeroList;
