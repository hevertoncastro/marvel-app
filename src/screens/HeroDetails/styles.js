import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const Container = styled.SafeAreaView`
  background-color: ${colors.dark};
  flex: 1;
  justify-content: flex-start;
  align-items: center;
`;

export const HeroDetailsHolder = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding-top: 24px;
  padding-left: 24px;
  padding-right: 24px;
`;

export const HeroDetailsImage = styled.Image`
  width: 360px;
  height: 360px;
  border-radius: 24px;
  background-color: ${colors.dark};
`;

export const HeroDetailsTitleHolder = styled.View`
  align-items: flex-start;
  flex-direction: row;
  justify-content: flex-start;
  width: 100%;
`;

export const HeroDetailsTitle = styled.Text`
  font-size: 18px;
  color: ${colors.white};
  font-weight: bold;
  margin-top: 16px;
  margin-bottom: 16px;
  align-self: flex-start;
`;

export const HeroDetailsText = styled.Text`
  font-size: 16px;
  color: ${colors.white};
  line-height: 28px;

  ${({ bold }) =>
    bold &&
    `
    font-weight: bold;
  `}
`;

export const HeroDetailsTabsHolder = styled.View`
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
  width: 100%;
  padding-top: 8px;
  /* padding-bottom: 8px; */
`;

export const HeroDetailsTab = styled.TouchableOpacity`
  border-width: 1px;
  border-style: solid;
  border-color: ${colors.lightWhite};
  color: ${colors.lightWhite};
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 16px;
  padding-right: 16px;
  margin-top: 8px;
  margin-right: 2px;
  border-top-right-radius: 8px;
  border-top-left-radius: 8px;

  ${({ active }) =>
    active &&
    `
    border-color: ${colors.lightWhite};
    background-color: ${colors.lightWhite};
    padding-top: 6px;
    padding-bottom: 6px;
  `}
`;

export const HeroDetailsTabText = styled.Text`
  font-size: 16px;
  color: ${colors.lightWhite};

  ${({ active }) =>
    active &&
    `
    color: ${colors.dark};
  `}
`;

export const HeroDetailsContentHolder = styled.View`
  justify-content: flex-start;
  width: 100%;
  border-width: 1px;
  border-style: solid;
  border-color: ${colors.lightWhite};
  padding: 12px 24px;
  border-top-right-radius: 15px;
  border-bottom-right-radius: 15px;
  border-bottom-left-radius: 15px;
`;
