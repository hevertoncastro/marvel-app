import React, { useState } from 'react';
import { ScrollView } from 'react-native';
import { Container } from './styles';
import StatusBar from '../../components/StatusBar';
import Header from '../../components/Header';
import ContentHolder from '../../components/ContentHolder';
import getHeroAvatarImage from '../../utils/getHeroAvatarImage';
import {
  HeroDetailsHolder,
  HeroDetailsImage,
  HeroDetailsTitle,
  HeroDetailsText,
  HeroDetailsTabsHolder,
  HeroDetailsTab,
  HeroDetailsTabText,
  HeroDetailsContentHolder,
} from './styles';

const HeroDetails = ({ route, navigation }) => {
  // TODO: extract values
  const { hero } = route.params;
  const [selectedTab, setSelectedTab] = useState('comics');
  const placeHolderImage = require('../../assets/images/heroes.png');

  return (
    <Container>
      <StatusBar />
      <Header
        title={hero.name}
        back={() => navigation.goBack()}
        heroId={hero.id}
      />
      <ScrollView style={ContentHolder}>
        <HeroDetailsHolder>
          {!!hero.thumbnail && (
            <HeroDetailsImage
              source={getHeroAvatarImage(hero.thumbnail, placeHolderImage)}
              resizeMode="cover"
            />
          )}
          {!!hero.name && <HeroDetailsTitle>{hero.name}</HeroDetailsTitle>}
          {!!hero.description && (
            <HeroDetailsText>{hero.description}</HeroDetailsText>
          )}
          <HeroDetailsTabsHolder>
            {!!hero.comics?.available && (
              <HeroDetailsTab
                active={selectedTab === 'comics'}
                onPress={() => setSelectedTab('comics')}>
                <HeroDetailsTabText active={selectedTab === 'comics'}>
                  comics: {hero.comics?.available}
                </HeroDetailsTabText>
              </HeroDetailsTab>
            )}
            {!!hero.series?.available && (
              <HeroDetailsTab
                active={selectedTab === 'series'}
                onPress={() => setSelectedTab('series')}>
                <HeroDetailsTabText active={selectedTab === 'series'}>
                  series: {hero.series?.available}
                </HeroDetailsTabText>
              </HeroDetailsTab>
            )}
            {!!hero.stories?.available && (
              <HeroDetailsTab
                active={selectedTab === 'stories'}
                onPress={() => setSelectedTab('stories')}>
                <HeroDetailsTabText active={selectedTab === 'stories'}>
                  stories: {hero.stories?.available}
                </HeroDetailsTabText>
              </HeroDetailsTab>
            )}
          </HeroDetailsTabsHolder>

          {(hero.comics?.items.length > 0 ||
            hero.series?.items.length > 0 ||
            hero.stories?.items.length > 0) && (
            <HeroDetailsContentHolder>
              <HeroDetailsText bold>Some appearances:</HeroDetailsText>
              {hero.comics?.items.length > 0 &&
                selectedTab === 'comics' &&
                hero.comics?.items.map((item) => (
                  <HeroDetailsText key={item.name}>{item.name}</HeroDetailsText>
                ))}
              {hero.series?.items.length > 0 &&
                selectedTab === 'series' &&
                hero.series?.items.map((item) => (
                  <HeroDetailsText key={item.name}>{item.name}</HeroDetailsText>
                ))}
              {hero.stories?.items.length > 0 &&
                selectedTab === 'stories' &&
                hero.stories?.items.map((item) => (
                  <HeroDetailsText key={item.name}>{item.name}</HeroDetailsText>
                ))}
            </HeroDetailsContentHolder>
          )}
        </HeroDetailsHolder>
      </ScrollView>
    </Container>
  );
};

export default HeroDetails;
