import {
  REQUEST_HEROES,
  RECEIVE_HEROES,
  RECEIVE_MORE_HEROES,
  SET_FAVORITE_HEROES,
  ADD_FAVORITE_HERO,
  REMOVE_FAVORITE_HERO,
  SEARCH_HEROES,
  SORT_HEROES,
} from '../actions/types';

import config from '../settings/config';

const initialState = {
  heroes: [],
  filteredHeroes: [],
  favoriteHeroes: [],
  orderBy: config.HEROES_LIST.SORT_ASC,
  limit: config.HEROES_LIST.LIMIT,
  page: 0,
  search: 0,
  loading: false,
};

const heroesReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_HEROES:
      return {
        ...state,
        loading: true,
      };
    case RECEIVE_HEROES:
      return {
        ...state,
        heroes: [...action.heroes],
        orderBy: action.orderBy,
        search: action.search,
        limit: action.limit,
        page: action.page,
        loading: false,
      };
    case RECEIVE_MORE_HEROES:
      return {
        ...state,
        heroes: state.heroes.concat(action.heroes),
        orderBy: action.orderBy,
        search: action.search,
        limit: action.limit,
        page: action.page,
        loading: false,
      };
    case SET_FAVORITE_HEROES:
      return {
        ...state,
        favoriteHeroes: [...action.favoriteHeroes],
      };
    case ADD_FAVORITE_HERO: {
      let idAlreadyExists = state.favoriteHeroes.indexOf(action.heroId) > -1;
      let updatedFavorites = state.favoriteHeroes.slice();

      if (idAlreadyExists) {
        updatedFavorites = updatedFavorites.filter(
          (id) => id !== action.heroId,
        );
      } else {
        updatedFavorites.push(action.heroId);
      }

      return {
        ...state,
        favoriteHeroes: updatedFavorites,
      };
    }
    case REMOVE_FAVORITE_HERO:
      return {
        ...state,
        favoriteHeroes: state.favoriteHeroes.filter(
          (item) => item !== action.heroId,
        ),
      };
    case SEARCH_HEROES: {
      return {
        ...state,
        search: action.searchQuery,
      };
    }
    case SORT_HEROES: {
      return {
        ...state,
        orderBy: action.orderBY,
      };
    }
    default:
      return state;
  }
};

export default heroesReducer;
