const config = {
  search: '',
  API: {
    BASE_URL: 'https://gateway.marvel.com/v1/public/characters',
    KEY: '1ffb14ba50fefb3c5852df5f9309947c',
    TS: '1600102379',
    HASH: '76edd24f16904697619e81ba3fb2f8c9',
  },
  PRELOADER: {
    NAVIGATION_DELAY: 2000,
  },
  HEROES_LIST: {
    PLACEHOLDER_IMAGE: '../../assets/images/heroes.png',
    SEARCH_PLACEHOLDER: 'Search a character...',
    SEARCH_FEEDBACK: 'Nenhum personagem encontrado, tente novamente',
    SORT_ASC: 'name',
    SORT_DESC: '-name',
    LIMIT: 20,
    ON_END_THRESHOLD: 0.9,
  },
};

export default config;
