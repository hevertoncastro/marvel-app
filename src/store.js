import thunk from 'redux-thunk';
import { createStore, applyMiddleware, combineReducers } from 'redux';

// Reducers
import userReducer from './reducers/userReducer';
import heroesReducer from './reducers/heroesReducer';

// Combining reducers
const rootReducer = combineReducers({
  user: userReducer,
  heroes: heroesReducer,
});

// Configuring store
const configureStore = () => createStore(rootReducer, applyMiddleware(thunk));

export default configureStore;
