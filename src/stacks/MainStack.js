import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import PreLoad from '../screens/PreLoad';
import HeroesList from '../screens/HeroesList';
import HeroDetails from '../screens/HeroDetails';

const Stack = createStackNavigator();

export default () => (
  <Stack.Navigator
    initialRouteName="PreLoad"
    screenOptions={{ headerShown: false }}>
    <Stack.Screen name="PreLoad" component={PreLoad} />
    <Stack.Screen name="HeroesList" component={HeroesList} />
    <Stack.Screen name="HeroDetails" component={HeroDetails} />
  </Stack.Navigator>
);
