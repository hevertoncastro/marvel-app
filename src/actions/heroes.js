import fetch from 'cross-fetch';
import {
  REQUEST_HEROES,
  RECEIVE_HEROES,
  RECEIVE_MORE_HEROES,
  SET_FAVORITE_HEROES,
  ADD_FAVORITE_HERO,
  REMOVE_FAVORITE_HERO,
  SEARCH_HEROES,
  SORT_HEROES,
} from './types';
import { getResourceURL } from '../service';

function requestHeroes() {
  return {
    type: REQUEST_HEROES,
  };
}

function receiveHeroes(heroes, orderBy, search, limit, page) {
  return {
    type: RECEIVE_HEROES,
    heroes,
    orderBy,
    search,
    limit,
    page,
  };
}

function receiveMoreHeroes(heroes, orderBy, search, limit, page) {
  return {
    type: RECEIVE_MORE_HEROES,
    heroes,
    orderBy,
    search,
    limit,
    page,
  };
}

export const setFavoriteHeroes = (favoriteHeroes) => ({
  type: SET_FAVORITE_HEROES,
  favoriteHeroes,
});

export const addFavoriteHero = (heroId) => ({
  type: ADD_FAVORITE_HERO,
  heroId,
});

export const removeFavoriteHero = (heroId) => ({
  type: REMOVE_FAVORITE_HERO,
  heroId,
});

export function fetchHeroes(orderBy, search, limit, page) {
  return function (dispatch) {
    dispatch(requestHeroes());

    return fetch(getResourceURL(orderBy, search, limit, page))
      .then((response) => response.json())
      .then((json) => {
        if (page) {
          dispatch(
            receiveMoreHeroes(json.data.results, orderBy, search, limit, page),
          );
        } else {
          dispatch(
            receiveHeroes(json.data.results, orderBy, search, limit, page),
          );
        }
      });
  };
}

export const searchHeroes = (searchQuery) => ({
  type: SEARCH_HEROES,
  searchQuery,
});

export const sortHeroes = (orderBY) => ({
  type: SORT_HEROES,
  orderBY,
});
