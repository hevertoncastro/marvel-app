## About the project

This app was developed for a position at LuizaLabs using React Native (with React Native CLI), consuming data from [Marvel Developer API](https://developer.marvel.com/).

##### Some of the libraries used

- **react-redux** (Used for global state management)
- **redux-thunk** (Used for async actions using Redux)
- **@react-navigation/native** (Used for screens navigation)
- **react-native-svg** (Used to easily handle SVG icons)
- **styled-components** (Used for styling with CSS syntax)
- **@react-native-community/async-storage** (Used for saving and retrieving data)

You may find more information of the libraries used in `package.json`.

#### Application summary



**1. Preload screen:**
On this screen the favorite heroes are retrieved from the AsyncStorage and saved in the Redux store, then the app just syncs new favorite hero events.

![](docs/images/marvel-app-01.png)

**2. List screen:**
This screen access the endpoint [/characters](https://gateway.marvel.com/v1/public/characters), and brings the heroes, which will then be displayed to the user.
The list shows an icon with the avatar of every hero as well as some extra info, e.g in how many comics this hero has been on.
Also, there's a [favorite option](#favorite-your-heroes) available.

![](docs/images/marvel-app-02.png)


**3. Details screen:**
Shows the details of the hero.
The user can see a larger avatar image, as well as the information regarding the hero appearances in comics, series and stories; These work as tabs, and the user can access these tabs to find out more about the current hero, for example, in which comics this hero can be found.
There's a button on the top right corner where the user can [favorite the current hero](#favorite-your-heroes).

![](docs/images/marvel-app-04.png)

**Some features:**

##### Favorite Your Heroes:

The app offers an extension to the Marvel's API, which allows the user of the application to favorite his/her own heroes!
There's an heart icon on both screens (List and Details), and when the user clicks on this icon the hero is then stored as one of his/her favorites.
This mechanism uses the user's cache/storage, and it is then synced/matched with the response from the API; therefore, when a new call is made to the API we still keep our favorite heroes saved.

**Sorting Options**
The user is allowed to sort the list of heroes by name, either on ascending order or descending order.

**Search**
The user may filter the heroes displayed by their names.
For instance, if the user would like to find the hero "Captain America" easily, he/she can simply type "Capt" on the search box and hit the search icon.

![](docs/images/marvel-app-03.png)

**Workaround for avatar images**

Unfortunately, some avatars from some heroes are not available from the API's response...
There's a workaround for that, and when that happens the application will display a default image (`src/assets/images/heroes.png`), so the app can still work smoothly. 

---

## How to run the project

#### Installing dependencies
You will need Node, Watchman, the React Native command line interface, and Xcode.

While you can use any editor of your choice to develop your app, you will need to install Xcode in order to set up the necessary tooling to build your React Native app for iOS.

#### Node & Watchman
We recommend installing Node and Watchman using Homebrew. Run the following commands in a Terminal after installing Homebrew:

> brew install node
> brew install watchman

If you have already installed Node on your system, make sure it is Node **8.3** or newer.

Watchman is a tool by Facebook for watching changes in the filesystem. It is highly recommended you install it for better performance.

#### Packages and Libraries

Access the root directory and run the following command:

> yarn install

#### React Native Command Line Interface

React Native has a built-in command line interface. Rather than install and manage a specific version of the CLI globally, we recommend you access the current version at runtime using npx, which ships with Node.js. With npx react-native <command>, the current stable version of the CLI will be downloaded and executed at the time the command is run.


#### Running the application on iOS

> npx react-native run-ios

For Windows commands and further information, please access the [React Native's documentation page](https://reactnative.dev/docs/0.60/getting-started).


> If you can't get this to work, see the [Troubleshooting page](https://reactnative.dev/docs/0.60/troubleshooting#content).

---

## Roadmap

#### Tests

The application uses some libraries that requires additionally code or mocks configuration to work (React Navigation, Redux, AsyncStorage, for example), so the next step would be to work on the settings and mockups to test the app.

#### Loaders

The list of heroes must have a loader style while fetching new heroes, both when the first fetch happens and fetching new items on scroll.

#### Additionally features

Besides the limitations of the API, would be great to have more features in the app, such as listing only the favorited heroes, ordering by appearences in comics, etc...
